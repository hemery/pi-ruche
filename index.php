<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf8"/>
<link rel="stylesheet" type="text/css" href="index.css"/>
<link rel="stylesheet" type="text/css" href="tuile.css"/>
<link rel="stylesheet" type="text/css" href="messagebox.css"/>
<title><?php echo $info->title; ?></title>
</head>
<body>
<?php require "header.php";
require "tile.php"; 
require "messageManager.php";
function verifTableMesures()
{
    include "base.php";
    $data = $dbh->query("SHOW TABLES FROM base_piruche LIKE 'mesures'");
    if(!$data->rowCount()) {
        throw new Exception('Aucune mesure à afficher (table mesures non présente)');
    }
    $data = $dbh->query("SELECT * from mesures"); 
    if(!$data->rowCount()) {
        throw new Exception('Aucune mesure à afficher (aucune valeur présente dans la table mesures');
    }
}


try {
    verifTableMesures();
    $humidity = new Screen\TileSensors("Humidité", "hu_int", "%", "img/humidity.svg");
    $temp = new Screen\TileSensors("Température", "temp_int", "°C", "img/temperature.svg");
    $density = new Screen\TileSensors("Densité d'abeille", "nb_abeille", "%", "img/bee.svg");
    $pressure = new Screen\TileSensors("Poids", "pressure", "Pa", "img/poids.svg");
    $photo = new Screen\Tile("Surveillance Visuelle", "img/camera.svg", "../photos");
    ?><nav>
    <?php
    $filename = "config.json";
    $json_source = file_get_contents($filename);
    $obj = json_decode($json_source);
    if($obj->activate->humidity||!$json_source) {
        $humidity->insert();
    }
    if($obj->activate->temperature||!$json_source) {
        $temp->insert();
    }
    if($obj->activate->density||!$json_source) {
        $density->insert();
    }
    if($obj->activate->pressure||!$json_source) {
        $pressure->insert();
    }
    if($obj->activate->surveillance||!$json_source) {
        $photo->insert();
    }
    ?>
</nav>
    <?php
} catch (Exception $e)
{
    insertInfo($e->getMessage());
}    
require "version.php" ?>
</body>
</html>
