<?php
namespace Screen\tests\units;

require_once __DIR__ . '/../../mesures.php';

use atoum;

class Mesure extends atoum
{
    public function testGetTitle()
    {
        $this
            ->given($this->newTestedInstance("test.json"))
            ->then
            ->string($this->testedInstance->getTitle())
            ->isEqualTo("Luminosité");
    }
    public function testGetImg()
    {
        $this
            ->given($this->newTestedInstance("test.json"))
            ->then
            ->string($this->testedInstance->getImg())
            ->isEqualTo("img/luminosité.svg");
    }
}
?>
