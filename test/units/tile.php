<?php
namespace Screen\tests\units;

require_once __DIR__ . '/../../tile.php';

use atoum;

class Tile extends atoum
{
    public function testInsert()
    {
        $this
            ->given($this->newTestedInstance("Humidité", "humidity.svg", "localhost"))
            ->then
            ->output(
                function () {
                    $this->testedInstance->insert();
                }
            )
        ->contains("Humidité")
        ->contains("localhost")
        ->contains("humidity.svg");
    }
}
?>
