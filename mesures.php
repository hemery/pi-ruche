<?php

namespace Screen;

class Mesure
{
    private $_title;
    private $_imgpath;
    private $_table;
    private $_unit;

    function __construct($filename)
    {
        $this->loadJson($filename);
    }
    
    private function loadJson($filename)
    {
        SESSION_START();
        $_SESSION['file'] = $filename;
        $json_source = file_get_contents($filename);
        $obj = json_decode($json_source);
        $this->_title = $obj->title;
        $this->_imgpath = $obj->img;
        $this->_table = $obj->table;
        $this->_unit = $obj->unit;
    }
    function getLastMeasured()
    {
        include "base.php";
        $sql = 'SELECT * from mesures ORDER BY `date` DESC, `heure` DESC';
        $row = $dbh->query($sql);
        $data = $row->fetch();
        return $data[$this->_table] . $this->_unit;
    }    

    function getTitle()
    {
        return $this->_title;
    }

    function getImg()
    {
        return $this->_imgpath;
    }

    function displayTable()
    {
        $this->startTable();
        $this->headerTable();
        $this->bodyTable();
        $this->stopTable();
    }

    function bodyTable()
    {
        include "base.php";

        echo "<tbody>";

        $sql = 'SELECT * from mesures';

        foreach  ($dbh->query($sql) as $row) {
            print "<tr>";

            $date = date_create($row['date']);
            $this->item(date_format($date, 'd/m/Y'));
            $this->item($row['heure']);
            $this->item($row[$this->_table]);

            print "</tr>";
        }

        echo "</tbody>";

    }

    function headerTable()
    {
        echo "<thead>";
        echo "<tr>";
        $this->item("Date");
        $this->item("Heure");
        $this->item($this->getTitle());
        echo "</tr>";
        echo "</thead>";
    }

    function item($name)
    {
        echo "<th>";
        echo $name;
        echo "</th>";
    }

    function startTable()
    {
        echo "<table>";
    }

    function stopTable()
    {
        echo "</table>";
    }


}
?>
