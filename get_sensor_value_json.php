<?php

header('Content-type: application/json');
require "base.php";

SESSION_START();

$filename = $_SESSION['file'];
$json_source = file_get_contents($filename);
$obj = json_decode($json_source);
        
$sql = 'SELECT * from mesures';
$requete = $dbh->query($sql);
$data = $requete->fetchAll(PDO::FETCH_ASSOC);

$date = array_column($data, 'date');
$value = array_column($data, $obj->table);

function prepare_chart($name,$unit, $datesArray, $measurementsArray,$color)
{
    $title = $name . " ". "(" . $unit . ")";
    $test = array(
        "title" => array("text" => $title),
        "xAxis" => array("data" => $datesArray),
        "yAxis" => array("type" => "value"),
        "series" => array(
            "type" => "line",
            "data" => $measurementsArray,
    "areaStyle" => "{}",
            "itemStyle" => array("color" => $color))
    
    );
    return json_encode($test);
}

$json = prepare_chart($obj->title, $obj->unit, $date, $value, "rgb(0, 120, 255)");

echo $json;
?>
