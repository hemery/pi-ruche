<?php

function saveDataInDatabase($humidity, $temperature, $nbBee, $pressure)
{
    include "../base.php";
    $dbh->query('CREATE TABLE mesures (temp_int INT, hu_int INT, nb_abeille INT, pressure INT, date DATE, heure TIME)');
    $sql = "INSERT INTO `mesures` (hu_int, temp_int, nb_abeille, pressure, date, heure) VALUES (:hu_int, :temp_int, :nb_bee, :pressure, CURDATE(), CURTIME())";

    if(!$prepExecute = $dbh->prepare($sql)) {
        print_r($dbh->ErrorInfo());
    }

    if(!$prepExecute->execute(
        array(
        ':hu_int' => $humidity,
        ':temp_int' => $temperature,
        ':nb_bee' => $nbBee,
        ':pressure' => $pressure
        )
    )
    ) {
        print_r($prepExecute->ErrorInfo());
    }
}

?>
