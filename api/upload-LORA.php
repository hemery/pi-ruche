<?php
require "saveData.php";
$json = file_get_contents('php://input');
$obj = json_decode($json);

$humidity = $obj->{'payload_fields'}->{'relative_humidity_2'};
$temperature = $obj->{'payload_fields'}->{'temperature_1'};
$pressure = $obj->{'payload_fields'}->{'barometric_pressure_10'}; 
$nbBee = 0;

saveDataInDatabase($humidity, $temperature, $nbBee, $pressure);
?>
