# Présentation
Pi@Ruche est un site web permettant de suivre l'évolution de l'humidité et la température mesurer dans une ruche connecté.

# Documentation
La documentation pour les développeurs est disponible [ici](doc/developer/developer.adoc) et celle pour les utilisateur [ici](doc/user/user.adoc).
