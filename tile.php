<?php

namespace Screen; 

class Tile
{
    protected $_name;
    protected $_url;
    protected $_img;

    protected function startTile()
    {
        return "<!-- Tuile " . $this->_name . '--><div class="tuile">';
    }


    protected function insertUrl()
    {
        return '<a href="' . $this->_url . '">';
    }

    protected function insertName()
    {
        return '<h2>' . $this->_name . '</h2>';
    }

    protected function insertImg()
    {
        return '<img src="' . $this->_img . '"/>';
    }

    protected function stopTile()
    {
        return '</a></div>';
    }

    function __construct($name,$img,$url)
    {
        $this->_name = $name;
        $this->_url = $url;
        $this->_img = $img;
    }
    public function insert()
    {
          $result = $this->startTile();        
          $result .= $this->insertUrl();
          $result .= $this->insertName();
          $result .= $this->insertImg();
          $result .= $this->stopTile();
          echo $result;

    }


}

class TileSensors extends Tile
{
    private $_unit;
    private $_table;


    function getValue()
    {
            include "base.php";
            $row = $dbh->query('SELECT * from mesures ORDER BY `date` DESC, `heure` DESC');
            $data = $row->fetch();
            return $data[$this->_table];
    }

    function __construct($name,$table,$unit,$imgpath)
    {
        $this->_name = $name;
        $this->_url = "screen.php?page=" . strtolower($name);
        $this->_unit = $unit;
        $this->_table = $table;
        $this->_img = $imgpath;
        $filename = strtolower($name) . ".json";
        $tableau = ['title'=>$name, 'table'=>$table, 'unit'=>$unit,'img'=>$imgpath];
        $contenuJson = json_encode($tableau);
        $file = fopen($filename, 'w+');
        fwrite($file, $contenuJson);
        fclose($file);
    }

    private function insertSensors()
    {
        return '<strong>' . $this->getValue() . " " . $this->_unit .'</strong>';
    }

    public function insert()
    {
        $result = $this->startTile();        
        $result .= $this->insertUrl();
        $result .= $this->insertName();
        $result .= $this->insertImg();
        $result .= $this->insertSensors();
        $result .= $this->stopTile();

        echo $result;

    }
}
?>
