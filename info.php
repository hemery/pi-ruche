<head>
<meta charset="utf8"/>
<link rel="stylesheet" type="text/css" href="version.css"/>
<link rel="stylesheet" type="text/css" href="index.css"/>
<title><?php echo $info->title; ?> - Informations</title>
</head>
<body>
<?php require "header.php" ?>
<div class="about">
<h2>Informations</h2>
<h3>Versions</h3>
<h4><?php echo $info->title ?> - version <?php echo $info->version->global ?></h4>
<p>Site web - version <?php echo $info->version->software ?></p>
<p>Ensemble matériel - version <?php echo $info->version->hardware ?></p>
<p>Ensemble documentaire - version <?php echo $info->version->documentation ?></p>
<h3>Auteur</h3>
Simon Hemery
</div>
</body>
