<?php
function getInfoOnProgram()
{
    $file = "info.json";
    $json_source = file_get_contents($file);
    $info = json_decode($json_source);
    return $info;
}
$info = getInfoOnProgram();
?>

<header>
<a href="index.php">
<h1><?php echo $info->title; ?></h1>
</a>
</header>
