# Changelog

## v0.2
 * Ajout du pied de page affichant le numéro de version dans les pages affichants les mesures
 * Correction de la couleur des tuiles passage du bleu clair au bleu foncé
 * Ajout d'un bouton sur la balise h1 permettant de retourner à la page d'accueil sur les pages affichant les données mesurées
 * Amélioration de la présentation des pages affichant le tableau des mesures : d'humidités, de températures et de densités d'abeilles
 * Exclusion des .sql de git
 * Ajout de l'affichage dans le pied de page de la version du site web
 * Amélioration de la présentation du titre de la page d'accueil
 * Amélioration de la structure html de la page d'acceuil
 * Ajout des tuiles afin d'améliorer la présentation
 * Ajout du nom de projet en titre du site web
 * Exclusion des fichiers .json
 * Ajout d'images représentant les différentes mesures
 * Ajout du tableau affichant les valeurs mesurées
 * Ajout de la possibilité de pouvoir consulter la dernière valeur d'humidité, de température, de densité d'abeille
 * Ignore les fichier .json du dépôt git
