<head>
<?php
require "mesures.php";
$filename = $_GET["page"] . ".json";
$mesure = new Screen\Mesure($filename);

?>
<meta charset="utf8"/>
<link rel="stylesheet" type="text/css" href="index.css"/>
<script src="echarts.common.min.js"></script>
<title><?php echo $mesure->getTitle() ?></title>
</head>
<body>
<?php require "header.php" ?>
<?php require "barre_info.php" ?>
<!-- preparing a DOM with width and height for ECharts -->
<div id="main" ></div>
<a href="data.php">Voir en détails</a>
<?php require "version.php" ?>
<script type="text/javascript" src="chart.js"></script>
</body>
