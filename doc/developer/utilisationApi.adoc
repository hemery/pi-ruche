== Utilisation de l'api du site web

=== Sans fichier json

Vous pouvez envoyer les données de température, hygrométrie et de densité d'abeille récupérer de la ruche au site web grace à l'api en les passants via l'URL du site web.
---------
upload-WIFI.php?
hu_int=  #<1>
&temp_int=  #<2>
&nb_bee=  #<3>
&pressure= #<4>
---------

<1> L'humidité mesurée dans la ruche à un instant t
<2> La température intérieur de la ruche à un instant t
<3> La densité d'abeille à un instant t
<4> La poid de la ruche à instant t

Par exemple, si votre site web est en local donc à comme url `http://localhost` et que vous voulez envoyée 5 % d'hygrométrie, 20 °C en température intérieur, 30 % pour la densité d'abeilles et 1000 Pa pour le poid de la ruche.

Cela donnera :

 localhost/api/upload-WIFI.php?hu_int=5&temp_int=20&nb_bee=30&pressure=1000

=== Avec fichier json par TTN

Il faut indiquée à TTN d'envoyé son fichier json avec cette structure à la variable data au fichier php *upload-LORA.php*:

[source,json]
----
{
"app_id":"loratest-0",
"dev_id":"ruche_plate",
"hardware_serial":"008798B7E56A7BF9",
"port":1,
"counter":0,
"is_retry":true,
"payload_raw":"AWf//wJo/wNn//8EaP8FZwPoBmjIB3MnEAhnA+gJaMgKcycQCwMnEAwDAAA=",
"payload_fields":               
	{
	"analog_out_11":100,
   	"analog_out_12":0,
	"barometric_pressure_10":1000,
	"barometric_pressure_7":1000,
	"relative_humidity_2":127.5, # <1>
	"relative_humidity_4":127.5,
	"relative_humidity_6":100,
	"relative_humidity_9":100,
	"temperature_1":-0.1, # <2>
	"temperature_3":-0.1,
	"temperature_5":100,
	"temperature_8":100
	},
"metadata":
	{
   	"time":"2019-06-25T16:04:32.670392251Z",
	"frequency":868.1,
	"modulation":"LORA",
	"data_rate":"SF7BW125",
	"coding_rate":"4/5",
	"gateways":[
		{
		"gtw_id":"eui-30aea4fffeec8af8",
		"timestamp":639773640,
		"time":"2019-06-25T16:04:32.566386Z",
		"channel":0,
		"rssi":-72,
		"snr":6,
		"rf_chain":0,
		"latitude":47.65939,
		"longitude":-2.7533996,
		"location_source":"registry"
		}
	]
},
----

<1> L'humidité mesurée dans la ruche à un instant t
<2> La température intérieur de la ruche à un instant t

Si on reprend notre exemple cela donne sa à rentrer sur TTN:

 localhost/api/upload-LORA.php
