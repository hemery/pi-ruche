== La page d'accueil
Sur la page d'accueil du site web vous pouvez voir plusieurs tuiles.

Une tuile est composées :

 * du nom de la grandeur mesurée
 * de la représentation visuel de cette grandeur
 * de la dernière grandeur mesurés.

image::tuile.png[200,200]

Par exemples ci-dessus on peut voir que la tuile représente l'humidité et que la dernière valeur mesurée est de 5%.

Par exemples sur l'image de la page d'accueil nous avons trois tuiles et nous pouvons voir que : 

 * l'humidité dans la ruche est de 80 %
 * la température est de 20 °C
 * la densité d'abeille de 10 %.
